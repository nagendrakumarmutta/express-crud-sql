const express = require("express");
const router = require("./routes/apis")
const connection = require("./connection")
const app = express();


app.use(express.json());
app.use("/api", router)


app.listen(3004, ()=> {
    console.log(`server is running at localhost::3004`)
});