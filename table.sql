CREATE TABLE user(
    id int not NULL AUTO_INCREMENT,
    username varchar(250),
    email varchar(250),
    password varchar(250),
    primary key(id)
);

CREATE TABLE refreshToken(
    id int not null AUTO_INCREMENT,
    username varchar(250),
    token text,
    primary key(id)
);