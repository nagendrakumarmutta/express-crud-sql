const express = require("express");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const router = express.Router();
const connection = require("../connection");
const authenticateToken = require("../authenticateToken");
require("dotenv").config();

function generateAccessToken(payload){
   return jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET, {expiresIn:"1m"});
}


router.post("/register", async(req, res)=>{
    const {username, email, password} = req.body;

    const isValidUsername = (username) => {
        const regex = /^[a-zA-Z ]{2,30}$/;
        return regex.test(username)
    }
    const isValidEmail = (email) => {
        const regex = /^[a-z0-9]+@[a-z]+\.[a-z]{2,3}$/;
        return regex.test(email)
    }
    const isValidPassword = (password) => {
        return  ((password.length >= 6) && (password.length <= 16)) 
    }
    

    if (!isValidUsername(username)) {
        res.status(400).json({msg:`USERNAME: ${username} is not valid.`})
    } else if (!isValidEmail(email)) {
        res.status(400).json({msg:`EMAIL: ${email} is not valid.`})
    } else if (!isValidPassword(password)) {
        res.status(400).json({msg:"password length should lie between 6 to 16"})
    }else{
        
    const sqlQuery1 = `select username from user where username="${username}"`;

     const hashedPassword = await bcrypt.hash(password, 10);

     connection.query(sqlQuery1, (err, result)=> {
        if(!err){
            if(result.length == 0){
                const sqlQuery2 = `insert into user(username, email, password) values("${username}", "${email}", "${hashedPassword}")`;
                connection.query(sqlQuery2, (err) =>{
                    if(!err){
                        res.status(201).json({msg:"user registered successfully"});
                    }else{
                        res.status(500).json(err)
                    }
                })
                
            }else{
                res.status(401).json({msg:"invalid user/username already exists"})
            }
        }
     });
    }
});



router.post("/login", (req, res)=> {
    const {username, password} = req.body;
    const sqlQuery = `select * from user where username = "${username}"`
    
    connection.query(sqlQuery, async(err, data) => {
        if(!err){
           if(data.length !== 0){

           const hashedPassword = data[0].password;
           const result = await bcrypt.compare(password, hashedPassword);

           if(result){

            const payload = {
                username,
               }
               const jwtToken = generateAccessToken(payload);
               const refreshToken = jwt.sign(payload, process.env.REFRESH_TOKEN_SECRET);
               
               const sqlQuery1 = `insert into refreshToken(username, token) values("${username}", "${refreshToken}")`
               connection.query(sqlQuery1);

               res.status(200).json({jwtToken, refreshToken,})

           }else{
            res.status(401).json({msg: "invalid Password"})
           }
        }
        }else{
            res.status(500).json(err);
        }
    })
});



router.patch("/resetPassword", authenticateToken, (req, res) => {
     const {username, password, newPassword} = req.body;
     const sqlQuery = `select * from user where username = "${username}"`

     connection.query(sqlQuery, async(err, data) => {
        if(!err){
              if(data.length !==0){
                const foundUser = data[0].username
                if(foundUser){
                    const hashedPassword = data[0].password;
                    const result = await bcrypt.compare(password, hashedPassword);

                    if(result){
                        const newHashedPassword = await bcrypt.hash(newPassword, 10);
                        const sqlQuery1 = `update user set password = "${newHashedPassword}" where username = "${username}"`
                         
                        connection.query(sqlQuery1, (err) => {
                            if(!err){
                                res.status(200).json({msg:"Password updated successfully"});
                            }else{
                                res.status(500).json(err);
                            }
                        })
                    }else{
                        res.status(401).json({msg: "Invalid Password"})
                    }
                }else{
                    res.status(401).json({msg:"user does not exist"});
                }
              }
        }else{
            res.status(500).json(err);
        }
     })
});



router.post("/token", (req, res)=>{
    const refreshToken = req.body.token;
    if(refreshToken==null){
        return res.status(401).json({msg:"required token"})
    }
    const sqlQuery = `select token from refreshToken where token = "${refreshToken}"`
    connection.query(sqlQuery, (err, data)=>{
        if(!err){
               if(data.length!==0){
                jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (error, user) => {
                    if(error) return res.status(403).json({msg:"invalid token"})
                    const accessToken = generateAccessToken({username: user.username})
                    res.status(200).json({accessToken,});
                })
            
               }else{
                res.status(401).json({msg:"token not found in the database"})
               }
        }else{
            res.status(500).json({err})
        }
    })
});



router.delete('/logout', (req,res) => {
        const sqlQuery = `DELETE FROM refreshToken`
        connection.query(sqlQuery, (error) => {
            if (!error){
                res.status(200).json({msg:"Logout Successfull"})
            }else{res.status(400).json(error.message)}
        })
});



router.post("/create", authenticateToken, (req, res)=>{
    let {name, description, price} = req.body;
    const sqlQuery = `insert into product(name, description, price) 
    values("${name}", "${description}", "${price}")`;
    connection.query(sqlQuery, (err, results)=> {
        if(!err){
            return res.status(200).json({message:"product Added Successfully"})
        }else{
            res.status(500).json(err)
        }
    })
});



router.get("/read", authenticateToken, (req, res) => {
    const sqlQuery = `select * from product`;
    connection.query(sqlQuery, (err, results)=>{
        if(!err){
            res.status(200).json(results)
        }else{
            res.status(500).json(err)
        }
    })
});



router.patch("/update/:id", authenticateToken, (req, res) => {
    const id = req.params.id;
    const {name, description, price} = req.body;
    const sqlQuery = `update product set name="${name}", description = "${description}", price="${price}" where id = ${id}`

    connection.query(sqlQuery, (err, results)=>{
        if(!err){
            if(results.affectedRows == 0){
                res.status(404).json({message:"id does not exist"})
            }else{
                res.status(200).json("updated successfully")
            }
        }else{
            res.status(500).json(err)
        }
    })
});



router.delete("/delete/:id", authenticateToken, (req, res) => {
    const id = req.params.id;
    const sqlQuery = `delete from product where id = ${id}`

    connection.query(sqlQuery, (err, results) => {
        if(!err){
            if(results.affectedRows ==0){
                res.status(404).json({message: "id does not exist"});
            }else{
                res.status(200).json({msg:"product deleted successfully"})
            }
        }else{
            res.status(500).json(err)
        }
    })
});




module.exports = router;