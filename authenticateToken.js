const jwt = require("jsonwebtoken")
require("dotenv").config();

const authenticateToken = (req, res, next) => {
    let authHeader = req.headers["authorization"]

    if(authHeader !== undefined){
       let jwtToken = authHeader.split(" ")[1]
       jwt.verify(jwtToken, process.env.ACCESS_TOKEN_SECRET, async(error) => {
          if(error){
              res.send("Invalid access token")
          }else{
            next()
        }
        })
} } 


module.exports = authenticateToken;